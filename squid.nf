#!/usr/bin/env Nextflow

process squid {
// require:
//   ALNS
//   params.squid$squid_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'squid_container'
  label 'squid'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/squid", pattern: "*fusion*"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(aln)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("${dataset}*fusion_predictions.abridged.coding_effect*"), optional: true, emit: fusions

  script:
  """
  /squid \
  ${parstr} \
  -b ${aln} \
  -o ${dataset}-${pat_name}-${run}
  """
}
